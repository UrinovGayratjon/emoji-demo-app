package uz.xsoft.emojidemotest.app

import android.app.Application
import android.graphics.Color
import androidx.emoji.text.EmojiCompat
import androidx.emoji.bundled.BundledEmojiCompatConfig

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        val config = BundledEmojiCompatConfig(baseContext)
        config.setEmojiSpanIndicatorEnabled(true)
        config.setEmojiSpanIndicatorColor(Color.YELLOW)
        EmojiCompat.init(config)
    }
}