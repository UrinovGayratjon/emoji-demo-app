package uz.xsoft.emojidemotest

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.emoji.text.EmojiCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val emojiContent: String = "Salom \uD83D\uDc83\ud83c\udffb\u1F610"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textViewWithoutEmoji.text = "Salom \uD83D\uDE10"

        EmojiCompat.get().registerInitCallback(object : EmojiCompat.InitCallback() {
            override fun onInitialized() {
                super.onInitialized()
                val processed = EmojiCompat.get().process("Salom \uD83D\uDE10")
                textViewWithEmoji.text = processed
            }

            override fun onFailed(throwable: Throwable?) {
                super.onFailed(throwable)
                Toast.makeText(this@MainActivity, throwable?.message ?: "", Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}
